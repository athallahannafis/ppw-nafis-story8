from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.contrib.auth import logout as logout_test
import requests

# Create your views here.

def books(request):
    print(dict(request.session))
    cont = {}
    if request.session.get("books") != None and \
    request.session.get("_auth_user_id") != None:
        cont["bookCount"] = request.session["books"]
    page = "books.html"
    print(cont)
    return render(request, page, cont)

def addCount(request):
    request.session["books"] = request.GET["count"]
    return HttpResponse("ntaps")

def getJson(request):
    source = "https://www.googleapis.com/books/v1/volumes?q=quilting"
    json_data = requests.get(source).json()
    return JsonResponse(json_data)

def getSearched(request, search):
    source = "https://www.googleapis.com/books/v1/volumes?q=" + search
    json_data = requests.get(source).json()
    return JsonResponse(json_data)

def logout(request):
    logout_test(request)
    url = "/books/"
    return redirect(url)
