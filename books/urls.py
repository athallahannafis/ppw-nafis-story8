from django.conf.urls import url
from django.urls import path
from .views import books, getJson, getSearched, logout, addCount


urlpatterns = [
    path("", books, name="home_books"),
    path("getJson/", getJson),
    path("search/<str:param>", getSearched),
    path("logout/", logout),
    path("addC/", addCount),
]