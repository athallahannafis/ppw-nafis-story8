from django.test import TestCase, Client

# Create your tests here.

class story8_books_test(TestCase):
    def test_url_exist(self):
        response = Client().get("/books/")
        self.assertEqual(response.status_code, 200)

    def test_url_json(self):
        response = Client().get("/books/getJson/")
        self.assertEqual(response.status_code, 200)
    
