## Repo for story8 PPW

## Status pipeline
[![pipeline status](https://gitlab.com/athallahannafis/ppw-nafis-story8/badges/master/pipeline.svg)](https://gitlab.com/athallahannafis/ppw-nafis-story8/commits/master)

## Status code coverage
[![coverage report](https://gitlab.com/athallahannafis/ppw-nafis-story8/badges/master/coverage.svg)](https://gitlab.com/athallahannafis/ppw-nafis-story8/commits/master)


## Web link
story8-athallah.herokuapp.com