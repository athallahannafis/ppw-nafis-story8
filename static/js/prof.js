var acc = document.getElementsByClassName("accordion");
var i;

// source -> w3school for accordion
$(document).ready(function() {
    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
        })
    }
})

// put js here
$(document).ready(function(){
    $("[data-toogle='tooltip']").tooltip();
})

// $(document).ready(function() {
//     $("#toggle1").click(function() {
//         // $(".container").css("background-color", "white");
//         $("#test").slideToggle("slow");
//     })
// })

$(document).ready(function() {
    $("#mode-change").click(function() {
        $("body").toggleClass("bg-change");
        $(".card").toggleClass("white-font");
        $(".accordion").toggleClass("dark-accordion");
        $(".panel").toggleClass("dark-panel");
        $(".active").toggleClass("dark-active");
        $(".menu").toggleClass("darkNav")
    })
})

$(document).ready(function() {
    $("#toggle2").click(function() {
        $("html, body").animate({
            scrollTop: $(".section2").offset().top
        }, 1000)
    })
})

// $(document).ready(function() {
//     $("body").css("visibility", "hidden");
//     $("#loading").css("visibility", "visible");
//     setTimeout(function() {
//         $("#loading").css("visibility", "hidden");
//         $("body").css("visibility", "visible");
//     }, 2000 )
// })