function addStar(object) {
    var counter = $(".favTotal").text()
    if (object.hasClass("fa-star-o")) {
        var newStar = parseFloat(counter) + 1
        object.removeClass("fa-star-o").addClass("fa-star");
    } else {
        var newStar = parseFloat(counter) - 1
        object.removeClass("fa-star").addClass("fa-star-o");
    }
    $(".favTotal").text(newStar);

    $.ajax ({
        url: "/books/addC/",
        data: {
            count : counter.innerHTML
        },
        success: function(response) {
            console.log(response);
        }
    })
}



$(document).ready(function() {
    // var search_str = $("#searchField").val();
    // $("#searchButton").click(function() {
    //     console.log("test");
    //     var u = "/books/search/" + search_str;
    //     if (search_str != "") {
    //         $.ajax ({
    //             url: u,
    //             success: function(result) {
    //                 console.log("AJAX RUN");
    //                 // for each
    //                 $('.booklist').html("");
    //                 $.each(result.items, function(i, item) {
    //                     $("<tr>").append(
    //                         $('<td>').text(i+1),
    //                         $('<td>').append('<img src=' + item.volumeInfo.imageLinks.thumbnail + "'/>"),
    //                         $('<td>').text(item.volumeInfo.title),
    //                         $('<td>').text(item.volumeInfo.authors),
    //                         $('<td>').text(item.volumeInfo.publisher),
    //                         $('<td>').text(item.volumeInfo.publishedDate),
    //                         $('<td>').append('<a><i class="fav fa fa-star-o"></i></a>'),
    //                     ).append(".booklist");
    //                 })
    //             }
    //         })
    //     }
    // })
    // // $("tbody").delegate(".fav", "click", function() {
	// // 	addStar($(this));  
    // })

    $(function() {
        $.ajax ({
            url: "/books/getJson",
            success: function(result) {
                // for each
                $.each(result.items, function(i, item) {
                    $("<tr>").append(
                        $('<td>').text(i+1),
			        	$('<td>').append('<img src=' + item.volumeInfo.imageLinks.thumbnail + "'/>"),
			            $('<td>').text(item.volumeInfo.title),
			            $('<td>').text(item.volumeInfo.authors),
			            $('<td>').text(item.volumeInfo.publisher),
			            $('<td>').text(item.volumeInfo.publishedDate),
                        $('<td>').append('<a><i class="fav fa fa-star-o"></i></a>'),
                    ).appendTo(".booklist");
                })
            }
        })
    })
    $("tbody").delegate(".fav", "click", function() {
		addStar($(this));
	})
})