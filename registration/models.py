from django.db import models

# Create your models here.
class form_model(models.Model):
    name = models.CharField(max_length = 50)
    email = models.EmailField(unique = True, null = True,  error_messages = {"unique" : "Email already exist"})
    password = models.CharField(max_length = 50)