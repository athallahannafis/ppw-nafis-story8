from django.conf.urls import url
from django.urls import path
from .views import registPage, regist, subList_page
# import views

urlpatterns = [
    path("", registPage),
    path("add/", regist),
    path("subscriber_list/", subList_page)
    # url(r"^$", registPage), 
    # url(r"^add/$", regist),
]