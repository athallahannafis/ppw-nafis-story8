from django.urls import resolve
from django.test import TestCase, Client

from .views import *

# Create your tests here.
class regist_unit_test(TestCase):
    def test_url_exist(self):
        response = Client().get("/registration/")
        self.assertEqual(response.status_code, 200)
