# Generated by Django 2.1.1 on 2018-11-19 06:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('registration', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='form_model',
            name='date',
        ),
        migrations.RemoveField(
            model_name='form_model',
            name='time',
        ),
        migrations.AddField(
            model_name='form_model',
            name='email',
            field=models.EmailField(error_messages={'unique': 'Email already exist'}, max_length=254, null=True, unique=True),
        ),
        migrations.AlterField(
            model_name='form_model',
            name='password',
            field=models.CharField(max_length=50),
        ),
    ]
