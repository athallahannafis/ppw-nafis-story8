from django import forms
from .models import form_model

class form(forms.ModelForm):

    name = forms.CharField(
        required = True,
        help_text = "50 characters max",
        widget = forms.TextInput(
            attrs = {
                "placeholder":"Full name",
                "id" : "name_field",
            }
        )
    )

    email_error = {
        "required": "Fill the email address.",
        "invalid": "Email not valid"
    }

    email = forms.EmailField(
        required = True,
        help_text = "Please type a valid email address",
        widget = forms.TextInput(
            attrs = {
                "placeholder":"Email",
                "id" : "email_field",
            }
        )
    )

    password = forms.CharField(
        required = True,
        widget = forms.PasswordInput(
            attrs = {
                "placeholder":"Password",
                "id" : "password_field"
            }
        )
    )


    class Meta:
        model = form_model
        fields = "__all__"