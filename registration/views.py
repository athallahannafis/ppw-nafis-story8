from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib import messages

from .models import form_model
from .forms import form

# Create your views here.

def registPage(request):
    f = form(request.POST or None)
    response = {"form" : f}
    page = "regist.html"
    return render(request, page, response)


def regist(request):
    path = "/registration/"
    if request.method == "POST":
        name = request.POST["name_input"]
        email = request.POST["email_input"]
        password = request.POST["password_input"]

        form_model.objects.create (
            name = name,
            email = email,
            password = password,
        )
        return HttpResponseRedirect(path)

def subList_page(request):
    return render(request, "sublist.html")

# def display_subList(request):
    

# def regist(request):
#     f = form(request.POST or None)
#     if request.method == "POST":
#         if f.is_valid():
#             f.save()
#     path = "/"
#     response = {"form" : f}
#     return HttpResponseRedirect(path, response)