from django.test import TestCase, Client
from django.urls import resolve

from .views import home

# Create your tests here.

class story8_unit_test(TestCase):
    def test_url_exist(self):
        response = Client().get("/")
        self.assertEqual(response.status_code, 200)